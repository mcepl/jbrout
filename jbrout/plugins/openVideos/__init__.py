#!/usr/bin/python
# -*- coding: utf-8 -*-

from __main__ import JPlugin
import os
import glob
import logging
from subprocess import Popen


class Plugin(JPlugin):
    """ Plugin to open the videos associated to selected files in
    totem/vlc/mplayer (linux/windows) """
    __author__ = "fchartier"
    __version__ = "1.0"

    LIST_VIDEO_SUFFIX = [".mpg", ".mpeg", ".mp4", ".avi", ".wma", ".mov", ".3gp", ".mts"]
    LIST_VIDEO_PLAYERS = ["totem", "vlc", "mplayer"]

    log = logging.getLogger(__name__)

    @JPlugin.Entry.PhotosProcess(_("Open associated videos"), order=2001, alter=False)
    def openAssociatedVideos(self, listOfPhotoNodes):
        """Open associated video of selected files"""
        listVideos = []

        for picture in listOfPhotoNodes:
            # self.showProgress(listOfPhotoNodes.index(picture),
            # len(listOfPhotoNodes), _("Searching videos") )

            picfile = picture.file
            # remove extension
            picshort = picfile[0:picfile.rfind(".")]
            # picshort = picfile[0:len(picfile)-4]
            self.log.debug(picfile + " => basename: " + picshort)

            # Search files starting like picture, but with all extensions
            listFic = glob.glob(picshort + "*")
            for fic in listFic:
                suffix = fic[fic.rfind("."):].lower()
                if suffix in self.LIST_VIDEO_SUFFIX:
                    self.log.info("Found video: " + fic)
                    listVideos.append(fic)
            # self.showProgress()

        self.openVideoList(listVideos)
        return False  # no visual modif

    @JPlugin.Entry.AlbumProcess(_("Open associated videos"), order=2001, alter=False)
    def openFolderVideos(self, node):
        """Open videos in selected folder"""

        # directly use a set to avoid duplicates ?
        listVideos = []

        # not recursive, we should get videos like photos, in current folder and all subfolders
        self.log.info("Searching videos in root folder [" + node.file + "]")
        listVideos += self.search_videos_in_folder(node.file)

        # extract list of folders from list of photos (nb will only get folders containing photos):
        all_folders = set([photo_node.folder for photo_node in node.getAllPhotos()])  # deduplicate folders
        for ifolder in all_folders:
            listVideos += self.search_videos_in_folder(ifolder)

        # Other possibility:
        # Search from recursive list of photos... but test perfs (add timing ?)
        for photo_node in node.getAllPhotos():
            listVideos += self.list_associated_videos(photo_node.file)

        # deduplicate videos with (but loses order => )
        listVideos = list(set(listVideos))
        listVideos.sort()

        self.openVideoList(listVideos)
        return False  # no visual modif

    def list_associated_videos(self, photo_fic):
        listVideos = []
        photo_basename = photo_fic[:photo_fic.rfind(".")]
        for suffix in self.LIST_VIDEO_SUFFIX:
            videoFic = photo_basename + suffix
            if os.path.exists(videoFic) and videoFic not in listVideos:
                self.log.info("Found video: %s for %s" % (videoFic, photo_fic))
                listVideos.append(videoFic)
        return listVideos

    def search_videos_in_folder(self, folder):
        self.log.info("Searching videos in [" + folder + "]")
        # Search files in folder
        listFic = os.listdir(folder)
        listFic.sort()
        listVideos = []
        for fic in listFic:
            suffix = fic[fic.rfind("."):].lower()
            # self.log.debug("suffix:" + suffix)
            if suffix in self.LIST_VIDEO_SUFFIX:
                videoFic = os.path.join(folder, fic)
                self.log.info("Found video: " + videoFic)
                listVideos.append(videoFic)

        return listVideos

    def openVideoList(self, list):
        """ try opening the list of videos with different players """
        if len(list) == 0:
            self.MessageBox(_("No video was found"))
        else:
            self.runListWith(self.LIST_VIDEO_PLAYERS, list)

    def runListWith(self, listCmd, listArgs):
        """ try command in the list 'listCmd' with the list listArgs """
        for c in listCmd:
            try:
                execList = [c]
                for a in listArgs:
                    execList.append(unicode(a))
                Popen(execList)

            except OSError:
                pass
            else:
                return True
        return False
